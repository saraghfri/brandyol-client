import './assets/main.css'

import { createApp } from 'vue'

import App from './App.vue'
import router from './router'
import axios from './plugins/axios';

const app = createApp(App)

app.use(router)

import 'vue-toastification/dist/index.css';
import Toast from 'vue-toastification';

const options: PluginOptions = {
    // You can set your default options here
};

app.use(Toast, {
    position: 'bottom-center',
    timeout: 5000,
    closeOnClick: true,
    pauseOnHover: false,
    draggable: true,
    draggablePercent: 0.6,
    showCloseButtonOnHover: false,
    hideProgressBar: false,
    closeButton: 'button',
});


app.use(Toast, options);


app.use(axios, {
    baseUrl: 'https://api.restful-api.dev/',
})

app.mount('#app')
