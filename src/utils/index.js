export function getBaseUrl() {
  let baseUrl = process.env.VUE_APP_BASE_API
  return `${baseUrl}/`
}