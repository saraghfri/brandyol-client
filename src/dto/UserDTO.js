// Example DTO for creating a new user
export default class CreateUserDTO {

    constructor(props) {
        this.id = props?.id || null ;
        this.name =  props?.name || null;
        this.capacity = props.data?.capacity || null;
        this.caseSize = props.data?.caseSize || null;
        this.color =  props.data?.color || null;
        this.cpuModel = props.data?.cpuModel || null;
        this.description = props.data?.description || null;
        this.generation = props.data?.generation || null;
        this.hardDiskSize = props.data?.hardDiskSize || null;
        this.price =  props?.price || null;
        this.screenSize = props.data?.screenSize || null;
        this.strapColour = props.data?.strapColour || null;
        this.year = props.data?.year || null;
    }

}

// Function to create a new user
// async function createUser(userData) {
//     try {
//         const response = await axios.post('/api/users', userData);
//         return response.data;
//     } catch (error) {
//         console.error('Error creating user:', error);
//         throw error;
//     }
// }